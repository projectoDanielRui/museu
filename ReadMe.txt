Bem vindo ao ReadMe do Projeto 33 . Catálogo do Museu de Física do ISEL.

Aqui vai poder encontrar as pastas pedidas com o conteúdo necessário a entrega final do projeto.

-------------------------------------------------------------------------------------------------------------------------
Como correr a aplicação localmente:
---------------------------------------------------------
Tecnologias necessaárias ao funcionamento da aplicação:
*Node.js - versao LTS - instalar também npm
*OpenJDK11 ou superior
*PostgreSQL13 - username: postgres password: root
-------------------------------------------------------------------------------------------------------------
Depois da instalação das ferramentas podemos prosseguir. siga os passos indicados.

1 - Abrir PGAdmin e criar uma base de dados chamada "museu".

2 - abrir eclipse, configurar uma "run configuration" - "Maven Build".

3 - deverá demorar alguns segundos a preencher a base dados e a afirmar que está a correr. Abrir browser no localhost:8080 

4 - verificar se a base de dados foi populada com tabelas e respectivo conteúdo. Se aparecer alguma tabela está a funcionar.

5 - login de user teste - username e password : user
-------------------------------------------------------------------------------------------------------------------------